package com.example.findbook;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class TersimpanBukuActivity extends AppCompatActivity {

    ListView list2;
    String[] namabuku2 = {
            "Dilan: dia adalah Dilanku tahun 1990",
            "Dilan: dia adalah Dilanku tahun 1990",
            "Dilan: dia adalah Dilanku tahun 1990"
    };

    int[] gambarbuku2 = {
            R.drawable.bukudilan,
            R.drawable.bukudilan,
            R.drawable.bukudilan
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tersimpan_buku);

        list2 = findViewById(R.id.list_view);

        ArrayAdapter adapter2 = new ArrayAdapter(TersimpanBukuActivity.this, android.R.layout.simple_list_item_2, namabuku2);
        list2.setAdapter(adapter2);
    }
}
