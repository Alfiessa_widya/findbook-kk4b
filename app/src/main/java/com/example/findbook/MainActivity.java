package com.example.findbook;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView list;
    String[] namabuku = {
            "Dilan: dia adalah Dilanku tahun 1990",
            "Dilan: dia adalah Dilanku tahun 1990",
            "Dilan: dia adalah Dilanku tahun 1990"
    };

    int[] gambarbuku = {
            R.drawable.bukudilan,
            R.drawable.bukudilan,
            R.drawable.bukudilan
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = findViewById(R.id.list_view);

        ArrayAdapter adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, namabuku);
        list.setAdapter(adapter);

        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent tersimpan = new Intent(MainActivity.this, TersimpanBukuActivity.class);
                startActivity(tersimpan);
            }
        });

        Button btn2 = findViewById(R.id.button2);
        btn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent buat = new Intent(MainActivity.this, BuatActivity.class);
                startActivity(buat);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent pindah = new Intent(MainActivity.this, HomeBukuActivity.class);
                pindah.putExtra(Konstanta.DATANAMA, namabuku[position]);
                pindah.putExtra(Konstanta.DATAGAMBAR, gambarbuku[position]);
                startActivity(pindah);
            }
        });
    }
}
