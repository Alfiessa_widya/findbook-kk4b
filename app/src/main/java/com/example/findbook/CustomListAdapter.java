package com.example.findbook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CustomListAdapter extends ArrayAdapter {
    private Context context;
    private String[] namabuku;
    private int[] gambarbuku;

    public CustomListAdapter(Context context, String[] namabuku, int[] gambarbuku) {
        super(context, R.layout.item_buku, namabuku);
        this.context = context;
        this.namabuku = namabuku;
        this.gambarbuku = gambarbuku;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater;
        View view = LayoutInflater.from(context).inflate(R.layout.item_buku, parent, false);

        TextView tvNamaBuku = view.findViewById(R.id.tv_item_nama);
        ImageView ivGambarBuku= view.findViewById(R.id.iv_item_gambar);

        tvNamaBuku.setText(namabuku[position]);
        ivGambarBuku.setImageResource(gambarbuku[position]);

        return view;
    }
}

